#!/bin/sh

rm -rf ./build
meson ./ ./build
cd ./build
echo "Building program..."
ninja
mv ./ReadNES3 ../
cd ../